from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import Status_Form
from .models import Status
# Create your views here.

def index(request):
	response = {}
	liststatus = Status.objects.all()
	response['liststatus'] = liststatus
	response['form'] = Status_Form
	if (request.method == 'POST'):
		# Create a form instance and populate it with data from the request (binding):
		form_jadwal = Status_Form(request.POST)
		# Check if the form is valid:
		if form_jadwal.is_valid():
			# process the data in form.cleaned_data as required (here we just write it to the model due_back field)
			form_jadwal.save()
			# redirect to a new URL
		else:
			messages.error(request, "pesan error")
		return HttpResponseRedirect(reverse('addstatus'))
    # If this is a GET (or any other method) create the default form.
	else:
		return render(request, 'landing.html', response)


def add_status(request):
	return HttpResponseRedirect(reverse('home'))

def profile(request):
	return render(request, "latjuga.html", {})

def story8(request):
	return render(request, "story8.html", {})