from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('addstatus/', add_status, name='addstatus'),
    path('profile/', profile, name='profile'),
    path('story8/',story8, name='story8'),
]