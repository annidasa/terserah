from django import forms
from .models import Status
# Create your models here.


class Status_Form(forms.ModelForm):
    status = forms.CharField(required=True,help_text='100 characters max.',widget=forms.TextInput(attrs={'class':'form-control',"placeholder":"apa kabar?"}))
    class Meta:
        model = Status
        fields = ('status',)