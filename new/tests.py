from django.test import TestCase
# Create your tests here.
from django.apps import apps
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_status, profile, story8
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from unittest import skip

class Story6Test(TestCase):
	def test_story6_is_exist(self):
		response = Client().get('') 
		self.assertEqual(response.status_code,200)

	def test_story6_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_story6_using_landing_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'landing.html')

	def test_add_status_success(self):
		response = Client().post('/', {"status": "idk"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(Status.objects.all().count(), 1)

		html_response = response.content.decode('utf8')
		self.assertIn("idk", html_response)

	def test_add_status_error(self):
		test = "a"*1000
		response = Client().post('/', {"status": test}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(Status.objects.all().count(), 0)
		

		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

class Challenge6Test(TestCase):
	def test_story6_is_exist(self):
		response = Client().get('/profile/') 
		self.assertEqual(response.status_code,200)

	def test_story6_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, profile)

	def test_story6_using_landing_template(self):
		response = Client().get('/profile/')
		self.assertTemplateUsed(response, 'latjuga.html')

class Story8Test(TestCase):
	def test_story8_is_exist(self):
		response = Client().get('/story8/') 
		self.assertEqual(response.status_code,200)

	def test_story8_using_index_func(self):
		found = resolve('/story8/')
		self.assertEqual(found.func, story8)

	def test_story8_using_landing_template(self):
		response = Client().get('/story8/')
		self.assertTemplateUsed(response, 'story8.html')
