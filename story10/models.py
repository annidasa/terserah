from django.db import models

class User_Data(models.Model):
    nama_lengkap = models.CharField(max_length=100)
    email = models.EmailField(unique=True, null=True, error_messages={"unique":"Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain"})
    password = models.CharField(max_length=16)
