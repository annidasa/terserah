from django.urls import path
from .views import *

urlpatterns = [
    path('daftar/', daftar, name='daftar'),
    path('cekemail/', cekemail, name='cekemail'),
    path('receive-data/', receivedata, name='sendfile'),
    path('subsciber-get/',subscriber_get,name='subscriberget'),
    path('delete/',delete,name='delete')
]