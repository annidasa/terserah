from django.test import TestCase
from django.apps import apps
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import json

class Story10Test(TestCase):
    def test_story10_is_exist(self):
        response = Client().get('/daftar/') 
        self.assertEqual(response.status_code,200)
    
    def test_story10_using_index_func(self):
        found = resolve('/daftar/')
        self.assertEqual(found.func, daftar)

    def test_lafi(self):
        response = Client().post("/cekemail/", data=json.dumps({"email": "laper@lab.com"}), content_type="application/json")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(response_dict["message"], "belum terdaftar")
    
    def test_lafi_2(self):
        User_Data.objects.create(nama_lengkap="hehe", email="laper@lab.com", password="hehe")
        response = Client().post("/cekemail/", data=json.dumps({"email": "laper@lab.com"}), content_type="application/json")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(response_dict["message"], "pernah terdaftar")

    def test_kirim(self):
        response = Client().post("/receive-data/", data={"nama_lengkap":"makan","email": "laper@lab.com", "password":"password"})
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(response_dict["message"], "yey masuk")
        self.assertTrue(User_Data.objects.filter(email="laper@lab.com").exists())

    def test_load_data(self):
        User_Data.objects.create(nama_lengkap="hehe", email="laper@lab.com", password="hehe")
        response = Client().get("/subsciber-get/")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(response_dict[0]["email"], "laper@lab.com")

        response = Client().post("/delete/", data=json.dumps({"email": "laper@lab.com", "password":"password"}), content_type="application/json")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(response_dict["message"], "Passwordnya salah")

        response = Client().post("/delete/", data=json.dumps({"email": "laper@lab.com", "password":"hehe"}), content_type="application/json")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(response_dict["message"], "Sukses dihapus")
        self.assertEqual(User_Data.objects.all().count(), 0)

    
    
    
