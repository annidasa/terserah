from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
import json
from urllib.request import urlopen
from .forms import Search
from django.http import JsonResponse
from django.contrib.auth import logout as auth_logout
from django.views.decorators.csrf import csrf_exempt
import requests
def index(request):
    response = {}
    response['form'] = Search
    response['jumlah'] = request.session.get("jumlah", 0)
    return render(request, 'story9.html', response)

def logins(request):
    response = {}
    return render(request, 'login.html', response)

def logouts(request):
    request.session.flush()
    auth_logout(request)
    response = {}
    return HttpResponseRedirect('/login')

def book_list(request):
    key = request.GET.get("q", "quilting")
    url = "https://www.googleapis.com/books/v1/volumes?q=" + key
    respon = requests.get(url)
    data = respon.json()
    show=[]
    book_list=data["items"]
    for book in book_list:
        show.append({ "judul": book["volumeInfo"]["title"],"penulis": book["volumeInfo"].get("authors", ["Anonymous"]), "thumbnail":book["volumeInfo"].get("imageLinks", {}).get("smallThumbnail", ""),"ISBN13" :book["volumeInfo"]["industryIdentifiers"][0]["identifier"] }) 
    return JsonResponse(show,safe=False)

@csrf_exempt
def add_fav(request):
    if request.method == "POST":
        request.session["jumlah"] = request.session.get("jumlah", 0) + 1
        return JsonResponse({"jumlah": request.session["jumlah"]})

@csrf_exempt
def del_fav(request):
    if request.method == "POST":
        request.session["jumlah"] = request.session.get("jumlah", 0) - 1
        return JsonResponse({"jumlah": request.session["jumlah"]})