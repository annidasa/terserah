from django import forms


class Search(forms.Form):
    judul_film = forms.CharField(help_text='100 characters max.',widget=forms.TextInput(attrs={'class':'form-control',"placeholder":"Search Movie"}))
    class Meta:
        fields = ('judul_film')