from django.urls import path
from .views import *

urlpatterns = [
    path('book-list/', index, name='index'),
    path('login/', logins, name='login'),
    path('logout/', logouts, name='logout'),
    path('book-list-raw/',book_list, name='book_list'),
    path('add-fav/', add_fav, name='addfav'),
    path('del-fav/', del_fav, name='del-fav'),

]