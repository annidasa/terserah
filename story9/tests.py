from django.test import TestCase
from django.apps import apps
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.contrib.auth import get_user_model
import time

class Story9Test(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('lol', 'lol@gmail.com', 'lol')

    def test_story9_is_exist(self):
        response = Client().get('/book-list/') 
        self.assertEqual(response.status_code,200)
    
    def test_story9_using_index_func(self):
        found = resolve('/book-list/')
        self.assertEqual(found.func, index)
    
    def test_story9_using_landing_template(self):
        client = Client()
        client.login(username="lol", password="lol")
        response = client.get('/book-list/')
        self.assertTemplateUsed(response, 'story9.html')
        self.assertIn("Hello, lol", response.content.decode("utf8"))
    
    def test_story9_jsondata_is_exist(self):
        response = Client().get('/book-list-raw/') 
        self.assertEqual(response.status_code,200)
    
    def test_story9_jsondata_using_book_list_func(self):
        found = resolve('/book-list-raw/')
        self.assertEqual(found.func, book_list)

    def test_loginpage_is_exist(self):
        response = Client().get('/login/') 
        self.assertEqual(response.status_code,200)

    def test_add_del(self):
        client = Client()
        response = client.post("/add-fav/")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(client.session["jumlah"], 1)

        response = client.post("/del-fav/")
        response_dict = json.loads(response.content.decode("utf-8"))
        self.assertEqual(client.session["jumlah"], 0)

    def test_login(self):
        client = Client()
        client.login(username="lol", password="lol")
        response = client.get('/logout/')
        self.assertEquals(len(dict(client.session)), 0)