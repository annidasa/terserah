function getBooks(q) {
  $.ajax({
    method: "GET",
    url: "/book-list-raw?q=" + q,
    dataType: "json",
    contentType: "application/json",
    success: function(data) {
      $(".books").remove();
      for (i=0;i<data.length;i++) {
        $("#listbuku").append(
           "<tr id='row-" + i + "' class='books'>"
           + "<td class='judul'>" + data[i].judul + "</td>"
           + "<td class='thumb'><img src='" + data[i].thumbnail + "'></td>"
           + "<td class='penulis'>" + data[i].penulis.join(", ") + "</td>"
           + "<td class='isbn'>" + data[i].ISBN13 + "</td>"
           + "<td class='star'><button class='star' id='fav-" + i + "' onclick=addToFav(" + i + ")>☆</button></td>"
           + "</tr>"
         )
      }
    },
  });
}

$(document).ready(function() {
  getBooks("quilting");
})

$("#submit").click(function() {
  getBooks(document.getElementById('id_judul_film').value);
})

function addToFav(i) {
  if (document.getElementById('fav-'+i).classList.contains('star')) {
    document.getElementById('fav-'+i).innerHTML="★";
    document.getElementById('fav-'+i).classList.remove('star');
    document.getElementById('fav-'+i).classList.add('staradd');
    $.ajax({
      method: "POST",
      url: "/add-fav/",
      success: function(data) {
        document.getElementById('counter').innerHTML="★: "+data.jumlah;
        }
      });
  }
  else{
    document.getElementById('fav-'+i).innerHTML="☆";
    document.getElementById('fav-'+i).classList.remove('staradd');
    document.getElementById('fav-'+i).classList.add('star');
    $.ajax({
      method: "POST",
      url: "/del-fav/",
      success: function(data) {
        document.getElementById('counter').innerHTML="★: "+data.jumlah;
        }
      });
  }
}
