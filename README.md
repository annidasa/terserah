
## Status pipeline
[![pipeline status](https://gitlab.com/annidasa/terserah/badges/master/pipeline.svg)](https://gitlab.com/annidasa/terserah/commits/master)

## Status code coverage
[![coverage report](https://gitlab.com/annidasa/terserah/badges/master/coverage.svg)](https://gitlab.com/annidasa/terserah/commits/master)
