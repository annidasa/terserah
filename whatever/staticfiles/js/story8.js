(function($) {
    
    var allPanels = $('.accordion > dd').hide();
      
    $('.accordion > dt > a').click(function() {
        $this = $(this);
        $target =  $this.parent().next();
        
      
        if($target.hasClass('active')){
          $target.removeClass('active').slideUp(); 
        }else{
          allPanels.removeClass('active').slideUp();
          $target.addClass('active').slideDown();
        }
        
      return false;
    });
  
  })(jQuery);

document.getElementById('lightButton').onclick = switchLight;
document.getElementById('darkButton').onclick = switchDark;

function switchLight() {
  document.getElementById('maincss').href = "/static/css/story8alter.css";
}

function switchDark() { 
  document.getElementById('maincss').href = "/static/css/story8.css";
}

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader-container").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
    